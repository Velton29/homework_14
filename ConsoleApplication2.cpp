﻿#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
	std::cout << "Enter your city: ";
	std::string city;
	std::getline(std::cin, city);

	std::cout << city << "\n";

	std::cout << city.length() << "\n";

	std::cout << city.front() << "\n";

	std::cout << city.back() << "\n";

	std::cin;
	return 0;
}